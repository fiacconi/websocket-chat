"use strict";

const env = require("./env/environment");
const app_factory = require("./server/server").factoryWebChatApp;

let app = app_factory(env);
app.start();