"use strict";


const initializeEnvironment = () => {
    let env = process.env.NODE_ENV.toLocaleLowerCase() || "dev";

    if ((env === "prod") || (env === "production")) {
        return process.env;
    }
    else {
        return require("./dev.json");
    }
};


module.exports = initializeEnvironment();
