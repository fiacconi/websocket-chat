"use strict";

const express = require("express");
const morgan = require("morgan");
const env = require("../env/environment");
const WebSocket = require("ws");
const moment = require("moment");


class WebChatServer {

    constructor(env) {
        this.env = env;
        this.server = null;
        this.wss = null;
        this.message_history = [];

        this.start = this.start.bind(this);
    }

    start() {
        // Build Express web app as static file server
        let app = express();
        app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status - :response-time ms'));
        app.use(express.static("src/frontend/build/"));
        
        // Store the server object
        this.server = app.listen(env.SERVER_PORT);

        // Bind WebSocket server to http server
        this.wss = new WebSocket.Server({server: this.server});

        // WebSocket server operation on connection
        this.wss.on('connection', (ws) => {

            // Bind message event
            ws.on("message", (data) => {
                // Append new message on history
                this.message_history.push(JSON.parse(data));
                this.message_history.slice(
                    this.message_history.length - 100, 
                    this.message_history.length + 1
                );

                // Broadcast message to other clients
                this.wss.clients.forEach((client) => {
                    if (client !== ws && client.readyState === WebSocket.OPEN) {
                        client.send(data);
                    }
                });

                console.log(`[${moment().format("DD/MMM/YYYY:HH:mm:ss ZZ")}] "WS message received and broadcasted"`)
            });

            // Send previous chat history
            ws.send(JSON.stringify(this.message_history));
            console.log(`[${moment().format("DD/MMM/YYYY:HH:mm:ss ZZ")}] "WS: connection enstablished"`);
        });
    }
}

module.exports.factoryWebChatApp = (env) => {
    return new WebChatServer(env);
}