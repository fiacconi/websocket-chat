import React from 'react';
import { Container, Form, Input, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label } from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import moment from 'moment';



class TitleSection extends React.Component {
  render() {
    return (
      <Container fluid className="App-title">
        WebSocket Chat {(this.props.name !== null) ? `- ${this.props.name}` : null}
      </Container>
    );
  }
}


class ChatNavigation extends React.Component {
  render() {
    return (
      <Container fluid className="chat-navigation" id="msgSpace">
        {this.props.chatHistory}
        <div ref={this.props.endRef} />
      </Container>
    );
  }
}


class SendMessageForm extends React.Component {
  render() {
    return (
      <Container fluid className="send-message-form">
        <Form>
          <Row form>
            <Col sm="10">
              <Input type="textarea" name="text" id="messageText" maxLength="1000" rows="2" className="send-message-text" />
            </Col>
            <Col sm="2" className="align-self-center py-2">
              <Button className="send-message-button" onClick={(e) => this.props.onClick()} color={null}>Send</Button>
            </Col>
          </Row>
        </Form>
      </Container>
    );
  }
}


class MessageDisplay extends React.Component {
  render() {
    return (
      <div style={{padding: "0.5rem", width: "100%"}}>
          <div className="msg-display">
            <span style={{fontSize: "0.7rem"}}>
              <span style={{color: this.props.msg.color}}><b>{this.props.msg.name}</b></span> @ {this.props.msg.timestamp}
            </span>
            <br/>
            {this.props.msg.msg}
          </div>
      </div>
    );
  }
}


class ChatNamePicker extends React.Component {

  constructor(props) {
    super(props);

    this.state = { disableButton: true };
    this.setShowButton = this.setShowButton.bind(this);
    this.onChosenName = this.onChosenName.bind(this);
  }

  setShowButton(e) {
    this.setState({
      disableButton: (e.target.value === "") ? true : false
    });
  }

  onChosenName(e) {
    let name = document.getElementById("insertName").value;
    this.props.nameSetter(name);
  }

  render() {
    return (
      <Modal isOpen={this.props.isOpen} backdrop="static" centered unmountOnClose labelledBy="prova" keyboard>
        <ModalHeader>
          Before you enter the chat ...
        </ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
            <Label for="insertName">Please insert the name you whant to use</Label>
            <Input 
              type="text" 
              name="insertName" 
              id="insertName" 
              onChange={this.setShowButton} />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button 
            className="send-message-button" 
            color={null} 
            disabled={this.state.disableButton} 
            onClick={this.onChosenName}>
            Start the conversation
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}


class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      clientName: null,
      clientColor: "#000",
      chatHistory: []
    };

    this.wsclient = null;
    this.endRef = React.createRef();

    this.getRandomColor = this.getRandomColor.bind(this);
    this.setWebSocketClient = this.setWebSocketClient.bind(this);
    this.setClientNameColor = this.setClientNameColor.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.addMessageToChat = this.addMessageToChat.bind(this);
    this.scrollToBottom = this.scrollToBottom.bind(this);
  }

  componentWillUnmount() {
    if (this.wsclient !== null) {
      this.wsclient.close();
    }
  }

  getRandomColor() {
    let r = Math.floor(Math.random() * 256);
    let g = Math.floor(Math.random() * 256);
    let b = Math.floor(Math.random() * 256);
    return `rgb(${r},${g},${b})`;
  }

  setWebSocketClient(name, color) {
    // Prepare the websocket client
    let WebSocket = window.MozWebSocket || window.WebSocket;
    let urlWs = `ws://${window.location.host}/wschat`;

    this.wsclient = new WebSocket(urlWs);

    // Handle errors
    this.wsclient.onerror = (e) => {
      console.error(`Error with ${e.target.url}`);
    };

    // Handle incoming messages
    this.wsclient.onmessage = (msg) => {

      let data = JSON.parse(msg.data);

      if (Array.isArray(data)) {
        this.setState({
          chatHistory: data.map((x, i) => <MessageDisplay key={i} msg={x}/>)
        }, () => this.scrollToBottom());
      }
      else {
        this.addMessageToChat(data);
      }
    };

    // Handle incoming signal of connection closure
    this.wsclient.onclose = (e) => {
      this.setState({ clientName: null });
    };
  }

  setClientNameColor(name) {
    let color = this.getRandomColor();

    this.setWebSocketClient(name, color);

    this.setState({
      clientName: name,
      clientColor: color
    });
  }

  addMessageToChat(message_to_send) {
    // Get current chat history
    let tmp = this.state.chatHistory;
    // Push new message
    tmp.push(<MessageDisplay key={tmp.length + 1} msg={message_to_send}/>);
    // Keep only last 100 messages and update the chat history
    this.setState({
      chatHistory: tmp.slice(tmp.length - 100, tmp.length + 1)
    }, () => this.scrollToBottom());
  }

  sendMessage() {

    let text = document.getElementById("messageText").value.trim();

    if (text !== "") {
      // Prepare message to send
      let message_to_send = {
        name: this.state.clientName,
        color: this.state.clientColor,
        timestamp: moment().format('YYYY-MM-DD hh:mm:ss'),
        msg: document.getElementById("messageText").value.trim()
      };

      // Send message to server
      this.wsclient.send(JSON.stringify(message_to_send));

      // Insert the message
      this.addMessageToChat(message_to_send);

      // Finally, clean up the text area
      document.getElementById("messageText").value = "";
    }
  }

  scrollToBottom() {
    this.endRef.current.scrollIntoView({ behavior: 'smooth' });
  }

  render() {
    return (
      <div className="App">
        <ChatNamePicker isOpen={(this.state.clientName === null)} nameSetter={this.setClientNameColor} />
        <TitleSection name={this.state.clientName}/>
        <ChatNavigation chatHistory={this.state.chatHistory} endRef={this.endRef} />
        <SendMessageForm onClick={this.sendMessage} />
      </div>
    );
  }
}


export default App;
