FROM node:latest

COPY . /app

WORKDIR /app

RUN ["npm", "install"]

WORKDIR /app/src/frontend

RUN ["npm", "install"]
RUN ["npm", "run", "build"]

WORKDIR /app

EXPOSE 5000

CMD ["npm", "start"]
