# WebSocket chat - A web chat based on WebSocket

This repository contains the implementation of a simple web chat based on websocket for real-time bidirectional communication.
The frontend is implemented with React.js; first it requires to enter an ID name, then it shows the chat board.
The backend is implemented in Node.js, combining an Express web app (mostly to serve the frontend static files) and a websocket server.
The chat history of 100 messages maximum is retained both by the frontend and the server; when a user connects the first time, it gets the history from the server to render on the chat board.
Every time a user enter a message, it is rendered on its own client, it is sent to the websocket server, and the browser broadcast the message to the other clients.
A Dockerfile and a docker-compose.yaml to run the application in a container is also provided.

## Repository structure

The repository is structured as follows.

```
websocket-chat/
|__ README.md
|__ package.json
|__ package-lock.json
|__ .gitignore
|__ .dockerignore
|__ Dockerfile
|__ docker-compose.yaml
|__ src/
    |__ app.js
    |__ env/
    |   |__ dev.json
    |   |__ environment.js
    |__ frontend 
    |   |__ package.json
    |   |__ package-lock.json
    |   |__ README.md
    |   |__ public/
    |   |   |__ favicon.ico
    |   |   |__ index.html
    |   |__ src/
    |       |__ App.css
    |       |__ App.js
    |       |__ index.css
    |       |__ index.js
    |       |__ serviceWorker.js
    |__ server/
        |__ server.js
```

## How to run the application

### Docker Compose

The easiest way is via Docker Compose by running

```bash
docker-compose up
```

This will build the `websocket-chat` service available on port 5000 at `localhost` or at the IP on the LAN of the machine running the container.

### Docker

If using simple Docker, you can first build the image.

```bash
docker build .
```
Then, you can run the container with the following command.
```bash
docker run --rm -p 5000:5000 .
```
The web app is available on port 5000 at `localhost` or at the IP on the LAN of the machine running the container.

### Manual set up

Assuming `node` and `npm` are installed and properly configured, the following sequence of command is required to set up dependecies and run the web app.

1. Enter in the repository directory.
2. Run `npm install`.
3. Enter in the frontend directory, `cd src/frontend/`.
4. Run `npm install`.
5. Run `npm run build` to build the frontend.
6. Go back to the repository directory.
7. Run `npm run dev` to run with configurations in `src/env/dev.json`.

The app should be listening at port 5000 on `localhost` or on the IP of the machine running the app on the LAN.